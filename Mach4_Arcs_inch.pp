+================================================
+                                                
+ Mach4 - Vectric machine output configuration file   
+                                                
+================================================
+                                                
+ History                                        
+                                                
+ Who      When       What                         
+ ======== ========== ===========================
+ Tony     15/11/2005 Written for metric
+ Tony     06/01/2006 Added ATC option      
+ Tony     14/05/2006 Fixed G21 mm in header   
+ Tony     24/07/2006 Added G2 & G3 Arc support + removed (( )) 
+ Tony     18/06/2007 Replaced the Tool comment     
+ Mark     14/08/2008 Added G1 to Feed moves,
+ Mark     28/08/2009 Added G91.1 to force use of incremental arcs
+ Mark     30/11/2009 Added TOOLPATHS_OUTPUT.
+ Brian    15/12/2009 Remove M05 from NEW_SEGMENT
+ Mark     18/01/2014 Added Dwell
+ Mark     24/10/2018 Added Helical arcs.
+ Charlotte 31/03/2020 Laser On/off commands added
+ Charlotte 15/07/2020 Removed new segment section which causes issue when lasering
+ MikePoling 15/07/2020 Modified to Inches and removed Z moves
+ MikePoling 06/09/2020 Modified Power Variable definition to incorporate M2003 command
+ SamMcCracken 07/25/2021 Added PWM control when laser turns on
+================================================

POST_NAME = "Mach4 Laser (Inches) (*.txt)"

FILE_EXTENSION = "txt"
LASER_SUPPORT = "YES"
UNITS = "INCHES"

DIRECT_OUTPUT = "Mach|Mach4.Document"

SUBSTITUTE = "({)}"

+------------------------------------------------
+    Line terminating characters                 
+------------------------------------------------

LINE_ENDING = "[13][10]"

+------------------------------------------------
+    Block numbering                             
+------------------------------------------------

LINE_NUMBER_START     = 0
LINE_NUMBER_INCREMENT = 10
LINE_NUMBER_MAXIMUM = 999999

+================================================
+                                                
+    Formating for variables                     
+                                                
+================================================

VAR LINE_NUMBER = [N|A|N|1.0]
VAR POWER = [P|A||1.0]
VAR SPINDLE_SPEED = [S|A|S|1.0]
VAR FEED_RATE = [F|C|F|1.1]
VAR X_POSITION = [X|C|X|1.3]
VAR Y_POSITION = [Y|C|Y|1.3]
VAR Z_POSITION = [Z|C|Z|1.3]
VAR ARC_CENTRE_I_INC_POSITION = [I|A|I|1.3]
VAR ARC_CENTRE_J_INC_POSITION = [J|A|J|1.3]
VAR X_HOME_POSITION = [XH|A|X|1.3]
VAR Y_HOME_POSITION = [YH|A|Y|1.3]
VAR Z_HOME_POSITION = [ZH|A|Z|1.3]
VAR SAFE_Z_HEIGHT = [SAFEZ|A|Z|1.3]
VAR DWELL_TIME = [DWELL|A|P|1.2]
+================================================
+                                                
+    Block definitions for toolpath output       
+                                                
+================================================

+---------------------------------------------------
+  Commands output at the start of the file
+---------------------------------------------------

begin HEADER

"( [TP_FILENAME] )"
"( File created: [DATE] - [TIME])"
"( for Mach4 from Vectric )"
"( Material Size)"
"( X= [XLENGTH], Y= [YLENGTH], Z= [ZLENGTH])"
"([FILE_NOTES])"
"(Toolpaths used in this file:)"
"([TOOLPATHS_OUTPUT])"
"(Tools used in this file: )"
"([TOOLS_USED])"
"G00G20G17G90G40G49G80"
"G70G91.1"
"m2003 (LASER_VECTOR_PWM_PERCENTAGE=100)"
"m2003 (LASER_VECTOR_FREQUENCY=10000)"
"m2003 (LASER_VECTOR_GATE_DELAY=0.2)"
"m2003 (LASER_VECTOR_GATE_DURATION=0.8)"
"m2004"
"G94"

+---------------------------------------------------
+  Command output after the header to switch spindle on
+---------------------------------------------------
 
begin SPINDLE_ON

"T[T]M06"
" ([TOOLNAME])"
"G00G43[ZH]H[T]"
"[S]M03"
"(Toolpath:- [TOOLPATH_NAME])"
"([TOOLPATH_NOTES])"
"[XH][YH][F]"

+---------------------------------------------------
+  Commands output for rapid moves 
+---------------------------------------------------

begin RAPID_MOVE

"G00[X][Y]"
"M63P5"


+---------------------------------------------------
+  Commands output for the first feed rate move
+---------------------------------------------------

begin FIRST_FEED_MOVE

"G1[X][Y][F]"


+---------------------------------------------------
+  Commands output for feed rate moves
+---------------------------------------------------

begin FEED_MOVE

"G1[X][Y]"

+---------------------------------------------------
+  Commands output for the first clockwise arc move
+---------------------------------------------------

begin FIRST_CW_ARC_MOVE

"G2[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for clockwise arc  move
+---------------------------------------------------

begin CW_ARC_MOVE

"G2[X][Y][I][J]"

+---------------------------------------------------
+  Commands output for the first counterclockwise arc move
+---------------------------------------------------

begin FIRST_CCW_ARC_MOVE

"G3[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for counterclockwise arc  move
+---------------------------------------------------

begin CCW_ARC_MOVE

"G3[X][Y][I][J]"

+---------------------------------------------------
+  Commands output for first clockwise helical arc  moves
+---------------------------------------------------

begin FIRST_CW_HELICAL_ARC_MOVE

"G2[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for clockwise helical arc  moves
+---------------------------------------------------

begin CW_HELICAL_ARC_MOVE

"G2[X][Y][I][J]"

+---------------------------------------------------
+  Commands output for first counterclockwise helical arc  moves
+---------------------------------------------------

begin FIRST_CCW_HELICAL_ARC_MOVE

"G3[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for counterclockwise helical arc  moves
+---------------------------------------------------

begin CCW_HELICAL_ARC_MOVE

"G3[X][Y][I][J]"

+---------------------------------------------------
+  Commands output for first clockwise helical arc plunge moves
+---------------------------------------------------

begin FIRST_CW_HELICAL_ARC_PLUNGE_MOVE

"G2[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for clockwise helical arc plunge moves
+---------------------------------------------------

begin CW_HELICAL_ARC_PLUNGE_MOVE

"G2[X][Y][I][J]"

+---------------------------------------------------
+  Commands output for first counter clockwise helical arc plunge moves
+---------------------------------------------------

begin FIRST_CCW_HELICAL_ARC_PLUNGE_MOVE

"G3[X][Y][I][J][F]"

+---------------------------------------------------
+  Commands output for counter clockwise helical arc plunge moves
+---------------------------------------------------

begin CCW_HELICAL_ARC_PLUNGE_MOVE

"G3[X][Y][I][J]"


+---------------------------------------------
+  Commands output for a dwell move
+---------------------------------------------

begin DWELL_MOVE

"G04 [DWELL]"

+---------------------------------------------------
+  Commands output when the jet is turned on
+---------------------------------------------------

begin JET_TOOL_ON

"m2003 (LASER_VECTOR_PWM_PERCENTAGE=[P])"
"m2004"
"M62P5"

+---------------------------------------------------
+  Commands output when the jet is turned off
+---------------------------------------------------

begin JET_TOOL_OFF

"M63P5"

+---------------------------------------------------
+  Commands output when the jet power is changed
+---------------------------------------------------

begin JET_TOOL_POWER

"m2003 (LASER_VECTOR_PWM_PERCENTAGE=[P])"
"m2004"

+---------------------------------------------------
+  Commands output at the end of the file
+---------------------------------------------------

begin FOOTER

+"G00[ZH]"
"G00[XH][YH]"
"M2005"
"M09"
"M30"
%


